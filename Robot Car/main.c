#include <stdio.h>

#define Motor11 0
#define Motor12 1
#define Motor21 2
#define Motor22 3

unsigned char forward (){
    unsigned char robot_control=0 ;
    robot_control|=(1<<Motor11);
    robot_control&=~(1<<Motor12);
    robot_control|=(1<<Motor21);
    robot_control&=~(1<<Motor22);
    return robot_control;

}
unsigned char backward (){
    unsigned char robot_control=0 ;
    robot_control|=(1<<Motor12);
    robot_control&=~(1<<Motor11);
    robot_control|=(1<<Motor22);
    robot_control&=~(1<<Motor21);
    return robot_control;
}

unsigned char right (){
    unsigned char robot_control=0 ;
    robot_control|=(1<<Motor11);
    robot_control&=~(1<<Motor12);
    robot_control&=~(1<<Motor21);
    robot_control&=~(1<<Motor22);
    return robot_control;
}

unsigned char left (){
    unsigned char robot_control=0 ;
    robot_control&=~(1<<Motor11);
    robot_control&=~(1<<Motor12);
    robot_control|=(1<<Motor21);
    robot_control&=~(1<<Motor22);
    return robot_control;
}
unsigned char stop (){
    unsigned char robot_control=0 ;
    robot_control&=~(1<<Motor11);
    robot_control&=~(1<<Motor12);
    robot_control&=~(1<<Motor21);
    robot_control&=~(1<<Motor22);
    return robot_control;
}


int main (){
char dir;
unsigned char control_signal;
for (;;){
    scanf("%s",&dir);
if (dir=='w'){
    control_signal=forward();
    printf("Move Robot Forward,control =%x\n",control_signal);
}

if (dir=='b'){
    control_signal=backward();
    printf("Move Robot Backward,control =%X\n",control_signal);
}

if (dir=='r'){
    control_signal=right();
    printf("Move Robot Right,control =%x\n",control_signal);
}
if (dir=='l'){
    control_signal=left();
    printf("Move Robot Left,control =%x\n",control_signal);
}

if (dir=='s'){
    control_signal=stop();
    printf("Move Robot Stop,control =%x\n",control_signal);
}
}
}
