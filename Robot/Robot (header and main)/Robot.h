#ifndef ROBOT_H_INCLUDED
#define ROBOT_H_INCLUDED

#define M11 0
#define M12 1
#define M21 2
#define M22 3

unsigned char Forward (){
    unsigned char RBC=0;
    RBC|=(1<<M11);
    RBC|=(1<<M21);
    return RBC;
}

unsigned char Backward (){
    unsigned char RBC=0;
    RBC|=(1<<M12);
    RBC|=(1<<M22);
    return RBC;
}

unsigned char Right (){
    unsigned char RBC=0;
    RBC|=(1<<M11);
    return RBC;
}

unsigned char Left (){
    unsigned char RBC=0;
    RBC|=(1<<M21);
    return RBC;
}

unsigned char Stop (){
    unsigned char RBC=0;
    return RBC;
}

#endif // ROBOT_H_INCLUDED
